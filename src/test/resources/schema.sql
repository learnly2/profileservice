CREATE TABLE IF NOT EXISTS customer
(
    id          varchar(100) not null primary key,
    fullName    varchar      not null,
    phoneNumber varchar(30) UNIQUE,
    createdBy   varchar(100) not null,
    createdAt   timestamp    not null,
    updatedAt   timestamp    not null
);