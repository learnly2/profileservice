package profile.service.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import profile.service.model.Customer;
import profile.service.repository.CustomerRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class CustomerServiceImplTest {

    @Mock
    CustomerRepository repository;

    @InjectMocks
    CustomerServiceImpl profileService;

    Customer mockProfile;

    @BeforeEach
    void setUp() {
        profileService = new CustomerServiceImpl(repository);
        mockProfile = new Customer();
        mockProfile.setCreatedBy("perminus12");
        mockProfile.setFullName("Perminus Gathanga");
        mockProfile.setPhoneNumber("0710625955");
        mockProfile.setId("perminus");
    }

    @AfterEach
    void tearDown() {
        mockProfile = null;
    }

    @Test
    void save() {
        Mockito.when(repository.save(any(Customer.class)))
                .thenReturn(Mono.just(mockProfile));

        Mono<Customer> user = repository.save(mockProfile);

        StepVerifier
                .create(user)
                .assertNext(newUser -> {
                    assertEquals("perminus12", newUser.getCreatedBy());
                    assertEquals("Perminus Gathanga", newUser.getFullName());
                    assertEquals("0710625955", newUser.getPhoneNumber());
                })
                .verifyComplete();
    }

    @Test
    void getOneByPhoneNumberTest() {
        Mockito.when(profileService.getOneByPhoneNumber(any(String.class)))
                .thenReturn(Mono.just(mockProfile));

        Mono<Customer> profile = profileService.getOneByPhoneNumber("0710625955");

        StepVerifier
                .create(profile)
                .assertNext(profile1 -> {
                    assertEquals("perminus12", profile1.getCreatedBy());
                    assertEquals("Perminus Gathanga", profile1.getFullName());
                    assertEquals("0710625955", profile1.getPhoneNumber());
                    assertEquals("perminus", profile1.getId());
                })
                .verifyComplete();
    }

    @Test
    void getOptionalByPhoneNumber() {
        Mockito.when(profileService.getOneByPhoneNumber("0710625955"))
                .thenReturn(Mono.just(mockProfile));

        Mono<Customer> profile = profileService.getOptionalByPhoneNumber("0710625955");

        StepVerifier.create(profile)
                .expectNext(mockProfile)
                .expectComplete()
                .verify();

        Mockito.when(profileService.getOneByPhoneNumber("0711625955"))
                .thenReturn(Mono.empty());

        Mono<Customer> result2 = profileService.getOptionalByPhoneNumber("0711625955");

        StepVerifier.create(result2)
                .expectNextMatches(Objects::nonNull)
                .expectComplete()
                .verify();
    }

    @Test
    void getAllCustomers() {
        List<Customer> mockProfiles = new ArrayList<>();
        mockProfiles.add(mockProfile);

        Mockito.when(profileService.getAllCustomers()).thenReturn(Flux.fromIterable(mockProfiles));
        Flux<Customer> profileFlux = profileService.getAllCustomers();

        StepVerifier
                .create(profileFlux)
                .expectNext(mockProfiles.get(0))
                .verifyComplete();
    }
}