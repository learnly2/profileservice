package profile.service.repository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.r2dbc.DataR2dbcTest;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import profile.service.model.Customer;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataR2dbcTest
@TestPropertySource(
        locations = "classpath:application.properties")
@ActiveProfiles("test")
class ProfileRepositoryTest {

    @Autowired
    private R2dbcEntityTemplate template;

    @Autowired
    private CustomerRepository profileRepository;

    Customer testProfile;

    @BeforeEach
    void setUp() {
        testProfile = new Customer();
        testProfile.setId("perminus");
        testProfile.setFullName("Perminus Gathanga");
        testProfile.setPhoneNumber("0710625955");
        testProfile.setCreatedBy("perminus12");

        // Insert the profile into the database
        template.insert(testProfile).block();
    }

    @AfterEach
    void tearDown() {
        template.delete(testProfile).block();
        testProfile = null;
    }

    @Test
    void testGetByPhoneNumber() {
        // Call the getByPhoneNumber() method
        Mono<Customer> resultMono = profileRepository.getByPhoneNumber("0710625955");

        // Use StepVerifier to verify the result
        StepVerifier.create(resultMono)
                .assertNext(newUser -> {
                    assertEquals("perminus12", newUser.getCreatedBy());
                    assertEquals("Perminus Gathanga", newUser.getFullName());
                    assertEquals("0710625955", newUser.getPhoneNumber());
                    assertEquals("perminus", newUser.getId());
                })
                .expectComplete()
                .verify();

        Mono<Customer> emptyResultMono = profileRepository.getByPhoneNumber("");

        StepVerifier.create(emptyResultMono)
                .expectComplete()
                .verify();
    }
}