package profile.service.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.r2dbc.R2dbcAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.reactive.server.WebTestClient;
import profile.service.model.Customer;
import profile.service.service.CustomerServiceImpl;
import reactor.core.publisher.Flux;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@WebFluxTest
@TestPropertySource(
        locations = "classpath:application.properties")
@ActiveProfiles("test")
@EnableAutoConfiguration(exclude = R2dbcAutoConfiguration.class)
class AdminControllerTest {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private CustomerServiceImpl customerService;

    @WithMockUser(roles = {"ADMIN"})
    @Test
    void getCustomerTest() {
        Customer customer1 = new Customer();
        customer1.setCreatedBy("perminus");
        customer1.setFullName("Perminus Gathanga");
        customer1.setPhoneNumber("0720625955");
        customer1.setId("customer_1");

        Customer customer2 = new Customer();
        customer2.setCreatedBy("perminus");
        customer2.setFullName("Perminus Gathanga");
        customer2.setPhoneNumber("0720625955");
        customer2.setId("customer_2");

        Flux<Customer> mockCustomers = Flux.just(customer1, customer2);
        when(customerService.getAllCustomers()).thenReturn(mockCustomers);

        // Send a GET request to the /customers endpoint
        webTestClient
                .get()
                .uri("/api/v1/profile/customers")
                .header("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Customer.class)
                .hasSize(2)
                .consumeWith(response -> {
                    List<Customer> customers = response.getResponseBody();
                    assert customers != null;
                    assertFalse(customers.isEmpty());
                    assertEquals(2, customers.size());
                });
    }
}