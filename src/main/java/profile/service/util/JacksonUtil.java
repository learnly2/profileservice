package profile.service.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;
import java.io.InputStream;

@Component
public class JacksonUtil {
  private ObjectMapper mapper = new ObjectMapper();

  public <T> T deserializeObject(String json, TypeReference<T> typeReference) {
    try {
      return mapper.readValue(json, typeReference);
    } catch (Exception e) {
      throw new RuntimeException("Could not deserialize and object!", e);
    }
  }

  public <T> T deserializeObject(InputStream stream, TypeReference<T> typeReference) {
    try {
      return mapper.readValue(stream, typeReference);
    } catch (Exception e) {
      throw new RuntimeException("Could not deserialize and object!", e);
    }
  }
}
