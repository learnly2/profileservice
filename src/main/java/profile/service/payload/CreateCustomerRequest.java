package profile.service.payload;

import jakarta.validation.constraints.NotBlank;

public record CreateCustomerRequest(
        @NotBlank(message = "Customer name is required") String fullName,
        @NotBlank(message = "Customer phone number is required") String phoneNumber
) {
}
