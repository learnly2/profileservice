package profile.service.payload;

import profile.service.model.Customer;

public record FetchCustomerResponse(Customer customer, boolean available) {
}
