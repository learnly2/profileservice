package profile.service.service;

import profile.service.model.Customer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CustomerService {
    Mono<Customer> save(String userId, String fullName, String phoneNumber);

    Mono<Customer> getOneByPhoneNumber(String phoneNumber);
    Mono<Customer> getOne(String customerId);

    Mono<Customer> getOptionalByPhoneNumber(String phoneNumber);

    Flux<Customer> getAllCustomers();
}
