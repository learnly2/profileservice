package profile.service.service;

import org.springframework.stereotype.Service;
import profile.service.exception.DataViolationException;
import profile.service.exception.NotFoundException;
import profile.service.model.Customer;
import profile.service.repository.CustomerRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CustomerServiceImpl implements CustomerService {

    final CustomerRepository repository;

    public CustomerServiceImpl(CustomerRepository profileRepository) {
        this.repository = profileRepository;
    }

    @Override
    public Mono<Customer> save(String userId, String fullName, String phoneNumber) {
        return getOptionalByPhoneNumber(phoneNumber)
                .map(p -> {
                    p.setPhoneNumber(phoneNumber);
                    p.setFullName(fullName);
                    p.setCreatedBy(userId);
                    return p;
                })
                .flatMap(repository::save)
                .onErrorMap(ex -> new DataViolationException("DVE-001", ex));
    }

    @Override
    public Mono<Customer> getOneByPhoneNumber(String phoneNumber) {
        return repository.getByPhoneNumber(phoneNumber);
    }

    @Override
    public Mono<Customer> getOptionalByPhoneNumber(String phoneNumber) {
        return getOneByPhoneNumber(phoneNumber)
                .switchIfEmpty(Mono.just(new Customer()));
    }

    @Override
    public Flux<Customer> getAllCustomers() {
        return repository.findAll();
    }

    @Override
    public Mono<Customer> getOne(String customerId) {
        return repository
                .findById(customerId)
                .switchIfEmpty(Mono.error(new NotFoundException("NFE-001")));
    }
}
