package profile.service.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import profile.service.model.Customer;
import reactor.core.publisher.Mono;

public interface CustomerRepository extends ReactiveCrudRepository<Customer, String> {
    Mono<Customer> getByPhoneNumber(String phoneNumber);
}
