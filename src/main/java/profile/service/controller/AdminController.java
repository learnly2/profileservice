package profile.service.controller;

import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import profile.service.exception.BadRequestException;
import profile.service.exception.NotFoundException;
import profile.service.model.Customer;
import profile.service.payload.CreateCustomerRequest;
import profile.service.service.CustomerServiceImpl;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.security.Principal;

@RestController
@RequestMapping(value = "/api/v1/profile", produces = {"application/json; charset=utf-8"}, consumes = {"application/json"})
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AdminController {

    private final CustomerServiceImpl customerService;

    public AdminController(CustomerServiceImpl customerService) {
        this.customerService = customerService;
    }

    //TODO can be paginated
    @Operation(summary = "Fetch all customers")
    @GetMapping(value = "/customers")
    public Flux<Customer> addCustomer() {
        return customerService.getAllCustomers();
    }

    @Operation(summary = "Create customer record")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/customer")
    public Mono<Customer> createCustomer(
            @Valid @RequestBody CreateCustomerRequest request,
            @AuthenticationPrincipal Principal principal) {
        return customerService.save(principal.getName(), request.fullName(), request.phoneNumber());
    }

    @Operation(summary = "Fetch customer")
    @GetMapping("/customer")
    public Mono<Customer> getCustomer(
            @RequestParam(name = "phn", required = false) String phoneNumber,
            @RequestParam(name = "cid", required = false) String customerId
    ) {
        if (phoneNumber != null && !phoneNumber.isBlank()) {
            return customerService
                    .getOneByPhoneNumber(phoneNumber)
                    .switchIfEmpty(Mono.error(new NotFoundException("CNF-001")));
        } else if (customerId != null && !customerId.isBlank()) {
            return customerService.getOne(customerId);
        } else {
            return Mono.error(new BadRequestException("Either 'phn' or 'cid' parameter is required."));
        }
    }
}
